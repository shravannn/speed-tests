proc factors(n: int): int =
    var factor = 0
    for i in countup(1, n+1):
        if n mod i == 0:
            factor+=1

    return factor

proc prime(): void =
    for x in countup(1, 10000):
        if (factors(x)) == 2:
            echo(x)

prime()

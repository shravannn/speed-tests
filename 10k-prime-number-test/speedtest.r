get_factors <- function(num){
    factors <- c()
    for (i in 1:num) {
    if (num %% i == 0){
        factors <- append(factors, i)
    }
    }
    factors
}

is_prime_10K <- function() {
    for (i in 1:10000){
        if (length(get_factors(i)) == 2){
        print(i)
    }
    }
}

t1 = Sys.time()
is_prime_10K()
t2 = Sys.time()

print(t2-t1)
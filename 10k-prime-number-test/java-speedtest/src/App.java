public class App {
    public static void main(String[] args) {
        prime();
    }

    public static int factors(int n) {
        int factor = 0;
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                factor++;
            }
        }
        return factor;
    }

    public static void prime() {
        for (int x = 1; x < 10001; x++) {
            if (factors(x) == 2) {
                System.out.println(x);
            }
        }
    }
}

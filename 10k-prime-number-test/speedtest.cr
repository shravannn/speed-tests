def factors(n)
    factor = 0
    i = 1
    while i <= n
        if n % i == 0
            factor += 1
        end
        i += 1
    end
    return factor
end

def prime
    x = 1
    while x < 10000
        if factors(x) == 2
            puts x
        end
        x += 1
    end
end

prime()

fn factors(n: i32) -> i32 {
    let mut factor: i32 = 0;
    for i in 1..n+1 {
        if n % i == 0 {
            factor+=1;
        }
    }
    return factor;
}

fn primes() {
    for i in 1..10000 {
        if factors(i) == 2 {
            println!("{}", i);
        }
    }
}

fn main() {
    primes();
}

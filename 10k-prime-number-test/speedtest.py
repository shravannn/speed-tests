def factors(n):
    factor = 0
    for i in range(1, n+1):
        if n % i == 0:
            factor += 1

    return factor

def prime():
    for x in range(1, 10000):
        if (factors(x)) == 2:
            print(x)


if __name__ == "__main__":
    prime()

def factors(n)
    factor = 0
    for i in 1..n do
        if n % i == 0
            factor += 1
        end
    end
    return factor
end

def prime
    for x in 1..10000 do
        if factors(x) == 2
            puts x
        end
    end
end

prime()

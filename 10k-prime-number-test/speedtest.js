function factors(n) {
    let factor = 0
    for (let i = 1; i < n+1; i++) {
        if (n % i == 0) {
            factor++
        }
    }
    return factor
}

function prime() {
    for (let x = 1; x < 10000; x++) {
        if (factors(x) == 2) {
            console.log(x);
        }
    }
}

prime()

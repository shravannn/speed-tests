#include <iostream>


int get_num_factors(int number) {
    int factors = 0; 
    for (int i=1; i<=number; i++){
        if (number % i == 0){
            factors += 1;
        }
    }
    return factors;
}

void prime(){
    for (int i = 1; i <= 10000; i++){
        if(get_num_factors(i) == 2){
            std::cout << i << std::endl;
        }
    }
}

int main() {
    prime();
    return 0;
}

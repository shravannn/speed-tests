package main

import (
	"fmt"
)


func factors(n int) int {
	var factor int
	for i := 1; i <= n; i++ {
		if n % i == 0 {
			factor++
		}
	}

	return factor
}

func prime() {
	for i := 1; i <= 10000; i++ {
		if (factors(i)) == 2 {
			fmt.Println(i)
		}
	}
}

func main() {
	prime()
}

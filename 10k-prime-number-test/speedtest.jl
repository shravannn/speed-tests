function get_factors(num)
    factors = []
    for i in 1:num
        if num % i == 0
            push!(factors, i)
        end
    end
    return factors
end

function prime_10K()
    for i in 1:10000
        if length(get_factors(i)) == 2
            println(i)
        end
    end
end

prime_10K()

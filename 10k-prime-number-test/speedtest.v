fn main() {
	for x := 1; x <= 10000; x++ {
		if factors(x) == 2 {
			println(x)
		}
	}
}

fn factors(n int) int {
	mut factor := 0
	for i := 1; i <= n; i++ {
		if n % i == 0 {
			factor++
		}
	}
	return factor
}
#include <stdio.h>

int get_num_factors(int number) {
    int factors = 0; 
    for (int i=1; i<=number; i++){
        if (number % i == 0){
            factors += 1;
        }
    }
    return factors;
}

int main() {

  for (int i = 1; i <= 10000; i++){
      if(get_num_factors(i) == 2){
          printf("%d \n", i);
      }
  }
}   

def factors(x:int) -> int:
    f = 0
    for i in range(1, x + 1):
        if x % i == 0: f += 1

    return f


if __name__ == "__main__":
    x = 1
    y = 1
    while y <= 10001:
        if factors(x) == 2:
            print(x)
            y += 1

        x += 1
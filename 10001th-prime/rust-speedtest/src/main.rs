fn factors(n: i32) -> i32 {
    let mut factor: i32 = 0;
    for i in 1..n+1 {
        if n % i == 0 {
            factor+=1;
        }
    }
    return factor;
}

fn main() {
    let mut x: i32 = 0;
    let mut y: i32 = 0;

    while y <= 10001 {
        if factors(x) == 2 {
            println!("{}", x);
            y += 1;
        }
        x += 1
    }
}
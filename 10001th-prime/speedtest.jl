function factors(num)
    factors = 0
    for i in 1:num
        if num % i == 0
            factors += 1
        end
    end
    return factors
end

function main() 
    x = 1
    y = 1
    while y <= 10001
        if factors(x) == 2
            println(x)
            y += 1
        end
        x += 1
    end
end

main()
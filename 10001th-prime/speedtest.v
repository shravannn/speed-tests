fn main() {
	mut x := 1
	mut y := 1
	for y <= 10001 {
		if factors(x) == 2 {
			println(x)
			y += 1
		}
		x += 1
	}
}

fn factors(n int) int {
	mut factor := 0
	for i := 1; i <= n; i++ {
		if n % i == 0 {
			factor++
		}
	}
	return factor
}
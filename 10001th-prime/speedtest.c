#include <stdio.h>

int factors(int x) {
    int f = 0;
    for (int i = 1; i < x + 1; i++)
    {
        if (x % i == 0) {
            f++;
        }
    }
    return f;
}

int main ()
{
    int n = 1;
    int m = 1;
    while (m <= 10001)
    {
        if (factors(n) == 2) {
            printf("%d\n", n);
            m++;
        }
        n++;
    }
    
    return 0;
}
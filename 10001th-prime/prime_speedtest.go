package main

import ("fmt")

func factors(x int) int {
	var f int = 0
	for i := 1; i < x + 1; i++ {
		if x % i == 0 {f++}
	}
	return f
}

func main() {
	var n int = 1
	var m int = 1
	for m != 10001 {
		if factors(n) == 2 {
			fmt.Println(n)
			m++
		}
		n++
	}
}
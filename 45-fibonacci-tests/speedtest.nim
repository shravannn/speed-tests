import std/times

proc fib(n: int): int =
    if n <= 1:
        return n

    return fib(n-1) + fib(n-2)

let start = cpuTime()
for i in countup(0, 45):
    echo i, " ", fib(i)
    
let finish = cpuTime()
echo finish - start, " seconds"
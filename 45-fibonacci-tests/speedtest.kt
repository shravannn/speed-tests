fun main() {
    val start = System.currentTimeMillis()
    for (i in 0..45) {
        println("$i ${fib(i)}")
    }
    val finish = System.currentTimeMillis()
    println("${finish-start}ms")
}

fun fib(n: Int): Int {
    if (n <= 1) {
        return n
    }
    return fib(n-1) + fib(n-2)
}


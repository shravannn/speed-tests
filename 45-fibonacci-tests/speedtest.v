fn fib(f int) int {
	if f <= 1 {
		return f
	}
	return fib(f-1) + fib(f-2)
}

fn main() {
	for i := 0; i < 41; i++ {
		println("$i ${fib(i)}")
	}
}

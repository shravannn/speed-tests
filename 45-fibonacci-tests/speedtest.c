#include <stdio.h>
#include <time.h>

int fib(int n) {
    if (n <= 1) {
        return n;
    }
    return fib(n-1) + fib(n-2);
}

int main() {
    struct timespec start, finish;
    double elapsed;
    int i;
    clock_gettime(CLOCK_REALTIME, &start);
    for (i = 0; i < 46; i++) {
        printf("%d %d\n", i, fib(i));
    }
    clock_gettime(CLOCK_REALTIME, &finish);

    elapsed = (finish.tv_sec - start.tv_sec);
    elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    printf("%f",elapsed);
    return 0;
}
function fib(n)
    if n <= 1
        return n
    end

    return fib(n-1) + fib(n-2)
end

function main()
    for i in 0:35
        println(i, " ", fib(i))
    end
end

@time main()

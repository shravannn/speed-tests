use std::time::{Instant};

fn fib(n: i32) -> i32 {
    if n <= 1 {
        return n;
    }
    return fib(n-1) + fib(n - 2)
}

fn main() {
    let start = Instant::now();
    for i in 0..41 {
        println!("{} {}", i, fib(i));
    }
    let duration = start.elapsed();
    println!("{:?}", duration);
}


public class App {
    public static void main(String[] args) {
        for (int i = 0; i < 41; i++) {
            System.out.printf("%d %d\n", i, fib(i));
        }

    }

    public static int fib(int n) {
        if (n <= 1) {
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }
}

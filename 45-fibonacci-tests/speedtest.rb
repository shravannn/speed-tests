def fib(n)
    if n <= 1
        return n
    end
    return fib(n-1) + fib(n-2)
end

init = Time.now
for i in 0..46 do
    puts i, fib(i)
end
finish = Time.now
puts finish - init
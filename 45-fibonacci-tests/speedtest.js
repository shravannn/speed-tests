function fib(n) {
    if (n <= 1) {
        return n
    }
    return fib(n - 1) + fib(n - 2)
}


console.time("init")
for (let i = 0; i < 41; i++) {
    console.log(i, fib(i))
}
console.timeEnd("init")
